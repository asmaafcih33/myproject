# INSTALL UBUNTU
FROM node:8

# INSTALL LIBAIO1 & UNZIP (NEEDED FOR NODE-ORACLEDB)
RUN apt-get update \
 && apt-get install -y libaio1 \
 && apt-get install -y build-essential \
 && apt-get install -y unzip \
 && apt-get install -y curl

# ADD ORACLE INSTANT CLIENT (instant client zip should be in ./oracle/linux dir)
RUN mkdir -p opt/oracle
ADD ./oracle/linux/ .

# 18.3
RUN unzip instantclient-basic-linux.x64-18.3.0.0.0 -d /opt/oracle

ENV LD_LIBRARY_PATH="/opt/oracle/instantclient_18_3"

WORKDIR app

RUN chmod 777 -R /app 

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY package.json .
RUN npm install
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
COPY . .

EXPOSE 8080
CMD [ "npm", "start" ]