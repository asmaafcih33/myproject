const script=require('../script/order.script');
const databaseDao=require('../../common/dao/database.dao');
const logger=require('../../common/util/logger.util');
const _ = require('lodash');
const oracledb = require('oracledb');

module.exports.findByStatusIdAndAgentId=(bind)=> {
  return new Promise(async (resolve, reject) => {
  try{
      let query; 
      logger.info(`Order.dao : findByStatusIdAndAgentId : Started ` );
      let opts={
        autoCommit : true
      };
      query = [script.findOrdersByStatusIdAndAgentIdScript(bind)];
      logger.info(`Query : ${query}`);
      const result = await databaseDao.executeQurey(query,opts);
      logger.info(`Order.dao : findByStatusIdAndAgentId : End ` );
      resolve(result.rows);
    }catch(err){
      logger.error(`Order.dao : findByStatusIdAndAgentId : error ` );
      reject(err)     
    }
  });
}

module.exports.findByStatusIdAndAgentIdPage=(bind)=> {
  return new Promise(async (resolve, reject) => {
  try{
      logger.info(`Order.dao : findByStatusIdAndAgentIdPage : Started ` );
      let opts={
        autoCommit : true
      };
      let resultSize=parseInt(bind.limit,10)+parseInt(bind.offest,10)-1;
      let query =[script.findByStatusIdAndAgentIdPageScript(bind.statusId
                                                            ,bind.agentId,bind.offest,
                                                            resultSize,bind.orderBy
                                                            ,bind.ordering )];
      logger.info(`Query : ${query}`);
      const result = await databaseDao.executeQurey(query,opts);
      logger.info(`Order.dao : findByStatusIdAndAgentIdPage : End ` );
      resolve(result.rows);
  }catch(err){
    logger.error(`Order.dao : findByStatusIdAndAgentIdPage : error :: ` );
    reject(err)     
    }
  });
}
  
module.exports.getOrdersByOrderNumber=(bind)=> {
  return new Promise(async (resolve, reject) => {
  try{
      logger.info(`Order.dao : getOrdersByOrderNumber : Started ` );
      let opts={
        autoCommit : true
      };
      let query = [script.getOrdersByOrderNumberScript(bind)];
      logger.info(`Query : ${query}`);
      const result = await databaseDao.executeQurey(query,opts);
      logger.info(`Order.dao : getOrdersByOrderNumber : End ` );
      resolve(result.rows);
  }catch(err){
    logger.error(`Order.dao : getOrdersByOrderNumber : error ` );
    reject(err)     
    }
  }
  );
}

module.exports.getProductByOrderId=(bind)=> {
  return new Promise(async (resolve, reject) => {
  try{
      logger.info(`Order.dao : getProductByOrderId : Started` );
      let opts={
        autoCommit : true
      };
      let query = [script.getProductByOrderIdScript(bind)];
      logger.info(`Query : ${query}`);
      const result = await databaseDao.executeQurey(query,opts);
      logger.info(`Order.dao : getProductByOrderId : End ` );
      resolve(result.rows);
  }catch(err){
    logger.error(`Order.dao : getProductByOrderId : error ` );
    reject(err)     
    }
  }
  );
}

module.exports.getApprovedQuantity=(bind)=> {
  return new Promise(async (resolve, reject) => {
  try{
      logger.info(`Order.dao : getApprovedQuantity : Started ` );
      let opts={
        autoCommit : true
      };
      let query = [script.getApprovedQuantityScript(bind)];
      logger.info(`Query : ${query}`);
      const result = await databaseDao.executeQurey(query,opts);
      logger.info(`Order.dao : getApprovedQuantity : End ` );
      resolve(result.rows);
  }catch(err){
    logger.error(`Order.dao : getApprovedQuantity : error ` );
    reject(err)     
    }
  }
  );
}

module.exports.getOrdersById=(bind)=> {
  return new Promise(async (resolve, reject) => {
  try{
      logger.info(`Order.dao : getOrdersById : Started ` );
      let opts={
        autoCommit : true
      };
      let orderIds=bind.orderIds.toString();
      bind.orderIds=orderIds;
      let query = [script.getOrdersByIdScript(bind)];
      logger.info(`Query : ${query}`);
      const result = await databaseDao.executeQurey(query,opts);
      logger.info(`Order.dao : getOrdersById : End ` );
      resolve(result.rows);
  }catch(err){
    logger.error(`Order.dao : getOrdersById : error ` );
    reject(err)     
    }
  });
}

module.exports.updateOrderStatusAndLoginId=(bind)=> {
  return new Promise(async (resolve, reject) => {
  try{
      logger.info(`Order.dao : updateOrderStatusAndLoginId : Started ` );
      let opts={
                autoCommit : true
              };
      let orderIds=bind.orderIds.toString();
      bind.orderIds=orderIds;
      let query = [script.updateOrderStatusScript(bind)];
      logger.info(`Query : ${query}`);
      const result = await databaseDao.executeQurey(query,opts);
      logger.info(`Order.dao : updateOrderStatusAndLoginId : End ` );
      resolve(result.rowsAffected);
  }catch(err){
    logger.error(`Order.dao : updateOrderStatusAndLoginId : error ` );
    reject(err)     
    }
  });
}

module.exports.submitOrder=(bind)=> {
  return new Promise(async (resolve, reject) => {
  try{
      logger.info(`Order.dao : submitOrder : Started ` );
      let opts={
                autoCommit : true
              };
      let query = [script.submitOrderScript(bind)];
      logger.info(`Query : ${query}`);
      const result = await databaseDao.executeQurey(query,opts);
      logger.info(`Order.dao : submitOrder : End ` );
      resolve(result.rowsAffected);
  }catch(err){
    logger.error(`Order.dao : submitOrder : error ` );
    reject(err)     
    }
  }
  );
}

module.exports.findOrderProductByOrderIdAndProductId=(bind)=> {
  return new Promise(async (resolve, reject) => {
  try{
      logger.info(`Order.dao : findOrderProductByOrderIdAndProductId : Started ` );
      let opts={
        autoCommit : true
      };
      let query = [script.findOrderProductByOrderIdAndProductIdScript(bind)];
      logger.info(`Query : ${query}`);
      const result = await databaseDao.executeQurey(query,opts);
      logger.info(`Order.dao : findOrderProductByOrderIdAndProductId : End ` );
      resolve(result.rows);
  }catch(err){
    logger.error(`Order.dao : findOrderProductByOrderIdAndProductId : error ` );
    reject(err)     
    }
  }
  );
}

module.exports.updateOrderProductByOrderIdAndProductId=(bind)=> {
  return new Promise(async (resolve, reject) => {
  try{
      logger.info(`Order.dao : updateOrderProductByOrderIdAndProductId : Started ` );
      let opts={
        autoCommit : true
      };
     const bindParams={}
      bindParams.id = {
        dir: oracledb.BIND_OUT,
        type: oracledb.NUMBER
      }
      let query = [script.updateOrderProductByOrderIdAndProductIdScript(bind)];
      logger.info(`Query : ${query}`);
      const result = await databaseDao.executeQurey(query,opts,bindParams);
      logger.info(`Order.dao : updateOrderProductByOrderIdAndProductId : End ` );
      resolve(result.outBinds.id[0]);
    }catch(err){
    logger.error(`Order.dao : updateOrderProductByOrderIdAndProductId : error ` );
    reject(err)     
    }
  });
}

module.exports.createOrderProduct=(bind)=> {
  return new Promise(async (resolve, reject) => {
  try{
      logger.info(`Order.dao : createOrderProduct : Started ` );
      let opts={
                autoCommit : true
              };
     const bindObje={};
     bindObje.id = {
        dir: oracledb.BIND_OUT,
        type: oracledb.NUMBER
      }
      let query = [script.createOrderProductScript(bind)];
      logger.info(`Query : ${query}`);
      const result = await databaseDao.executeQurey(query,opts,bindObje);
      logger.info(`Order.dao : createOrderProduct : End` );
      resolve(result.outBinds.id[0]);
   }catch(err){
    logger.error(`Order.dao : createOrderProduct : error ` );
    reject(err)     
  }});
}

module.exports.deleteOrder=(bind)=> {
  return new Promise(async (resolve, reject) => {
  try{
   logger.info(`Order.dao : deleteOrder : Started ` );
     /**add find by orderId if not found then not execute queries */
      let query = [script.deleteOrderTransactionScript(bind),
                   script.deleteOrderProductScript(bind),
                   script.deleteOrderScript(bind)            
                  ];
      logger.info(`Query : ${query}`);
      const result = await databaseDao.executeQurey(query);
      logger.info(`Order.dao : deleteOrder : End ` );
      resolve(result.rowsAffected);
  }catch(err){
    logger.error(`Order.dao : deleteOrder : error ` );
    reject(err)     
    }
  });
}

module.exports.addOrder=(bind)=> {
  return new Promise(async (resolve, reject) => {
  try{
      logger.info(`Order.dao : addOrder : Started ` );
      let opts={
        autoCommit : true
      };
      let bindParam={}
      bindParam.id = {
        dir: oracledb.BIND_OUT,
        type: oracledb.NUMBER
      }
      let query = [script.createOrderScript(bind)];
      logger.info(`Query : ${query}`);
      const result = await databaseDao.executeQurey(query,opts,bindParam);
      logger.info(`Order.dao : addOrder : End ` );
      resolve(result.outBinds.id[0]);
  }catch(err){
    logger.error(`Order.dao : addOrder : error ` );
    reject(err)     
    }
  }
  );
}

module.exports.deleteProductByOrderProductId=(bind)=> {
  return new Promise(async (resolve, reject) => {
  try{
      logger.info(`Order.dao : deleteProductByOrderProductId : Started ` );
      let query = [script.deleteProductbyOrderProductIdScript(bind)];
      logger.info(`Query : ${query}`);
      let opts={
        autoCommit : true
      };
      const result = await databaseDao.executeQurey(query,opts);
      logger.info(`Order.dao : deleteProductByOrderProductId : End ` );
      resolve(result.rowsAffected);
  }catch(err){
    logger.error(`Order.dao : deleteProductByOrderProductId : error ` );
    reject(err)     
    }
  });
}