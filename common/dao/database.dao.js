const oracledb = require('oracledb');
const logger=require('../util/logger.util');
const constants= require('../../common/constant/business.constant')
const AppError = require('../../common/exception/app.exception');


module.exports.executeQurey =function (statements, opts = {},binds=[]) {
  return new Promise(async (resolve, reject) => {
    let conn;
    let result;
    try { 
        logger.info(`database.dao : executeQurey : Started ` );
        opts.outFormat = oracledb.OBJECT;
        conn = await oracledb.getConnection();
      /* execute multiple query in one transaction */
      for(let index=0; index< statements.length ; index++){
        result = await conn.execute(statements[index], binds, opts);
      }
      /**in case execute one query */
      if(opts.autoCommit ==undefined && !opts.autoCommit){
        conn.commit();
      }
      resolve(result);
      logger.info(`database.dao : executeQurey : Ended ` );
    } catch (err) {
      reject(new AppError('database.dao','executeQurey',
              constants.EXTERNAL_SERVER_ERROR_CODE,
              constants.EXTERNAL_SERVER_ERROR_KEY,
              err.message)
           );
      logger.info(`database.dao : executeQurey : error ` );
    } finally {
      if (conn) { // conn assignment worked, need to close
        try {
          await conn.close();
        } catch (err) {
          logger.log(err);
        }
      }
    }
  });
}
