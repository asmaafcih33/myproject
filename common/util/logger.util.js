
const { createLogger, format, transports } = require('winston');
require('winston-daily-rotate-file');
const appRoot = require('app-root-path');
const env = process.env.NODE_ENV || 'development';

const dailyRotateFileTransport = new transports.DailyRotateFile({
  filename: `${appRoot}/orderLogs/%DATE%-order.log`,
  datePattern: 'YYYY-MM-DD',
  json: true,
  maxsize: 5, //5MB
  maxFiles: 5,
  colorize: false,
  handleExceptions: true,
});

const logger = createLogger({
  // change level if in dev environment versus production
  /* { error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 } */
  level: 'info',
  format: format.combine(
    format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss'
    }),
    format.printf(info => `${info.timestamp} : ${info.level} : ${info.message}`)
  ),
  transports: [
    new transports.Console({
      level: 'info',
      format: format.combine(
        format.colorize(),
        format.printf(
          info => `${info.timestamp} ${info.level}: ${info.message}`
        )
      )
    }),
    dailyRotateFileTransport
  ],
  exitOnError: false
});


module.exports = logger;
module.exports.stream = {
    write: function(message, encoding){
        logger.info(message);
    }
};