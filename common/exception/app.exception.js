module.exports=class AppError extends Error {
    constructor(serviceName = '',operationName='',code,key, ...params) {
      // Pass remaining arguments (including vendor specific ones) to parent constructor
      super(...params);
      // Maintains proper stack trace for where our error was thrown (only available on V8)
       if (Error.captureStackTrace) {
        Error.captureStackTrace(this,this.constructor);
      } 
      // Custom debugging information
      this.key=key;
      this.code=code;
      this.serviceName=serviceName;
      this.operationName = operationName;
      this.date = new Date();
    }
  }
  
 