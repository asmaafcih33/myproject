const oracledb = require('oracledb');
const database = require('../services/database.js');

const baseQuery =
    `select ID "id",
            USER_NAME "user_name",
            IMAGE "image",
            USER_NUMBER "user_number",
            POSITION "position",
            LAST_NAME "last_name",
            FIRST_NAME "first_name"
    from users`;

async function find(context) {
  let query = baseQuery;
  const binds = {};

  if (context.id) {
    binds.employee_id = context.id;
    query += `\nwhere ID = :employee_id`;
  }
  const result = await database.simpleExecute(query, binds);

  return result.rows;
}

module.exports.find = find;

const createSql =
 `insert into users (
      ID,
      USER_NAME ,
      IMAGE ,
      USER_NUMBER,
      POSITION ,
      LAST_NAME ,
      FIRST_NAME
  ) values (
      :user_id,
      :user_name,
      :image,
      :user_number,
      :position,
      :last_name,
      :first_name
    ) returning  ID
  into :id`;

async function create(emp) {
  const employee = Object.assign({}, emp);

  employee.id = {
    dir: oracledb.BIND_OUT,
    type: oracledb.NUMBER
  }
  const result = await database.simpleExecute(createSql, employee);

  employee.user_id =result.outBinds.id[0];

  return employee;
}

module.exports.create = create;

const updateSql =
 `update users
      set USER_NAME = :user_name,
      IMAGE = :image,
      USER_NUMBER = :user_number,
      POSITION = :position,
      LAST_NAME = :last_name,
      FIRST_NAME = :first_name
  where ID = :user_id`;

async function update(emp) {
  const employee = Object.assign({}, emp);
  const result = await database.simpleExecute(updateSql, employee);
  if (result.rowsAffected && result.rowsAffected === 1) {
    return employee;
  } else {
    return null;
  }
}

module.exports.update = update;

const deleteSql =
 `begin
   delete from users
         where ID = :user_id;
       :rowcount := sql%rowcount;
  end;`

async function del(id) {
  const binds = {
    user_id: id,
    rowcount: {
      dir: oracledb.BIND_OUT,
      type: oracledb.NUMBER
    }
  }
  const result = await database.simpleExecute(deleteSql, binds);

  return result.outBinds.rowcount === 1;
}

module.exports.delete = del;
